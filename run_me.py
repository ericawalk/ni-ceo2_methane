#Eric A. Walker
#This script reads calculation results and produces summaries and plots.
import matplotlib
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 20})
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import extract_egy
from extract_egy import extract_egy
import extract_egy_ensemble
from extract_egy_ensemble import extract_egy_ensemble
import free_egy_correct
from free_egy_correct import free_egy_correct
import NIST_entropy_calc
from NIST_entropy_calc import NIST_entropy_calc
######################## Obtain free energies of DFT calculations:
T = 673.15
dict_ads = {
'CH4_not_ads': ['./CH4_ads_beta'],
'CH3_H': ['./CH3_OH94'],
'CH2_2H' : ['./CH2_H2O94'],
'CH2_vac' : ['./CH2_vacancy'],
'CH2' : ['./CH2'],
'CH_H' : ['./CH_OH'],
'C_2H' : ['./C_H2O'],
'C' : ['./C'],
'CO2' : ['./CO2'],
'NiCeO2': ['./S1']
}

dict_gas = {
'O2': ['./O2'],
'H2O': ['./water'],
'CO2': ['./CO2_gas']
}

dict_ads1 = {
'CH3_H': ['./Plot2/CH3'],
'CH2_2H': ['./Plot2/CH2'],
'methoxy': ['./Plot2/Methoxy']
}

dict_ads2 = {
'2O_CH3': ['./Plot3/2O_pathway/CH3'],
'2O_CH2': ['./Plot3/2O_pathway/CH2'],
'2O_CH': ['./Plot3/2O_pathway/CH'],
'2O_C': ['./Plot3/2O_pathway/C'],
'4O_CH3': ['./Plot3/4O_pathway/CH3'],
'4O_CH2': ['./Plot3/4O_pathway/CH2'],
'4O_CH': ['./Plot3/4O_pathway/CH'],
'4O_C': ['./Plot3/4O_pathway/C']
}

for key in dict_ads:
    if key == 'NiCeO2':
        free_energy = np.asarray(extract_egy(dict_ads[key]))
        free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads[key]))
        dict_ads[key].extend([free_energy, free_energy_ensemble])
    elif key == 'CH4_not_adsorbed':
        energy = np.asarray(extract_egy(dict_ads[key]))
        free_energy_ensemble = np.asarray(extract_egy_ensemble[key])
        ZPE = free_egy_correct(dict_ads[key], ads_or_gas = 'gas', T=T)
        CH4_gas_entropy = NIST_entropy_calc(T = T, A = -0.703029, B = 108.4773, C = -42.52157, D = 5.862788, E = 0.678565, F = -76.84376, G = 158.7163)
        free_energy = energy + ZPE - CH4_gas_entropy
        dict_ads[key].extend([free_energy,free_energy_ensemble])
    else:
        energy = np.asarray(extract_egy(dict_ads[key]))
        free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads[key]))
        free_energy = energy + free_egy_correct(dict_ads[key], T=T)
        dict_ads[key].extend([free_energy,free_energy_ensemble])

for key in dict_ads1:
    energy = np.asarray(extract_egy(dict_ads1[key]))
    free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads1[key]))
    free_energy = energy + free_egy_correct(dict_ads1[key], T=T)
    dict_ads1[key].extend([free_energy, free_energy_ensemble])

for key in dict_ads2:
    energy = np.asarray(extract_egy(dict_ads2[key]))
    free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads2[key]))
    free_energy = energy + free_egy_correct(dict_ads2[key], T=T)
    dict_ads2[key].extend([free_energy, free_energy_ensemble])

O2_gas_energy = np.asarray(extract_egy(dict_gas['O2']))
O2_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['O2']))
O2_gas_ZPE = free_egy_correct(dict_gas['O2'], ads_or_gas = 'gas')
O2_gas_entropy = NIST_entropy_calc(T = T, A = 30.03235, B = 8.772972, C = -3.988133, D = 0.788313, E = -0.741599, F = -11.32468, G = 236.1663)
O2_gas_free_energy = O2_gas_energy + O2_gas_ZPE - O2_gas_entropy
dict_gas['O2'].extend([O2_gas_free_energy, O2_gas_free_energy_ensemble])
print('O2_gas_energy',O2_gas_energy)
print('O2_gas_free_energy',O2_gas_free_energy)

CO2_gas_energy = np.asarray(extract_egy(dict_gas['CO2']))
CO2_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['CO2']))
CO2_gas_ZPE = free_egy_correct(dict_gas['CO2'], ads_or_gas = 'gas')
CO2_gas_entropy = NIST_entropy_calc(T = T, A = 24.99735, B = 55.18696, C = -33.69137, D = 7.948387, E = -0.136638, F = -403.6075, G = 228.2431)
CO2_gas_free_energy = CO2_gas_energy + CO2_gas_ZPE - CO2_gas_entropy
dict_gas['CO2'].extend([CO2_gas_free_energy, CO2_gas_free_energy_ensemble])
print('CO2_gas_energy',CO2_gas_energy)
print('CO2_gas_free_energy',CO2_gas_free_energy)

H2O_gas_energy = np.asarray(extract_egy(dict_gas['H2O']))
H2O_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['H2O']))
H2O_gas_ZPE = free_egy_correct(dict_gas['H2O'], ads_or_gas = 'gas')
H2O_gas_entropy = NIST_entropy_calc(T = T, A = 30.09200, B = 6.832514, C = 6.793435, D = -2.534480, E = 0.082139, F = -250.8810, G = 223.3967)
H2O_gas_free_energy = H2O_gas_energy + H2O_gas_ZPE - H2O_gas_entropy
dict_gas['H2O'].extend([H2O_gas_free_energy, H2O_gas_free_energy_ensemble])

print('H2O_gas_energy',H2O_gas_energy)
print('H2O_gas_free_energy',H2O_gas_free_energy)

# Construct free energy reaction pathway from DFT calculations and plot (figure 1).  Complete pathway dehydrogenation route without methoxy.
CH4_not_ads_1 = dict_ads['CH4_not_ads'][1] + 2*dict_gas['O2'][1]
CH3_H_2 = dict_ads['CH3_H'][1] + 2*dict_gas['O2'][1]
CH2_2H_3 = dict_ads['CH2_2H'][1] + 2*dict_gas['O2'][1]
CH2_vac_4 = dict_ads['CH2_vac'][1] + 2*dict_gas['O2'][1] + dict_gas['H2O'][1]
CH2_5 = dict_ads['CH2'][1] + 1.5*dict_gas['O2'][1] + dict_gas['H2O'][1]
CH_H_6 = dict_ads['CH_H'][1] + 1.5*dict_gas['O2'][1] + dict_gas['H2O'][1]
C_2H_7 = dict_ads['C_2H'][1] + 1.5*dict_gas['O2'][1] + dict_gas['H2O'][1]
C_8 = dict_ads['C'][1] + 1.5*dict_gas['O2'][1] + 2*dict_gas['H2O'][1]
CO2_9 = dict_ads['CO2'][1] + 0.5*dict_gas['O2'][1] + 2*dict_gas['H2O'][1]
NiCeO2_10 = dict_ads['NiCeO2'][1] + dict_gas['CO2'][1] + 2*dict_gas['H2O'][1]

CH4_not_ads_1_ensemble = dict_ads['CH4_not_ads'][2] + 2*dict_gas['O2'][2]
CH3_H_2_ensemble = dict_ads['CH3_H'][2] + 2*dict_gas['O2'][2]
CH2_2H_3_ensemble = dict_ads['CH2_2H'][2] + 2*dict_gas['O2'][2]
CH2_vac_4_ensemble = dict_ads['CH2_vac'][2] + 2*dict_gas['O2'][2] + dict_gas['H2O'][2]
CH2_5_ensemble = dict_ads['CH2'][2] + 1.5*dict_gas['O2'][2] + dict_gas['H2O'][2]
CH_H_6_ensemble = dict_ads['CH_H'][2] + 1.5*dict_gas['O2'][2] + dict_gas['H2O'][2]
C_2H_7_ensemble = dict_ads['C_2H'][2] + 1.5*dict_gas['O2'][2] + dict_gas['H2O'][2]
C_8_ensemble = dict_ads['C'][2] + 1.5*dict_gas['O2'][2] + 2*dict_gas['H2O'][2]
CO2_9_ensemble = dict_ads['CO2'][2] + 0.5*dict_gas['O2'][2] + 2*dict_gas['H2O'][2]
NiCeO2_10_ensemble = dict_ads['NiCeO2'][2] + dict_gas['CO2'][2] + 2*dict_gas['H2O'][2]

one = CH4_not_ads_1 - CH4_not_ads_1
two = CH3_H_2 - CH4_not_ads_1
three = CH2_2H_3 - CH4_not_ads_1
four = CH2_vac_4 - CH4_not_ads_1
five = CH2_5 - CH4_not_ads_1
six = CH_H_6 - CH4_not_ads_1
seven = C_2H_7 - CH4_not_ads_1
eight = C_8 - CH4_not_ads_1
nine = CO2_9 - CH4_not_ads_1
ten = NiCeO2_10 - CH4_not_ads_1

print('ten',ten)

one_std = np.std(CH4_not_ads_1_ensemble - CH4_not_ads_1_ensemble)
two_std = np.std(CH3_H_2_ensemble - CH4_not_ads_1_ensemble)
three_std = np.std(CH2_2H_3_ensemble - CH4_not_ads_1_ensemble)
four_std = np.std(CH2_vac_4_ensemble - CH4_not_ads_1_ensemble)
five_std = np.std(CH2_5_ensemble - CH4_not_ads_1_ensemble)
six_std = np.std(CH_H_6_ensemble - CH4_not_ads_1_ensemble)
seven_std = np.std(C_2H_7_ensemble - CH4_not_ads_1_ensemble)
eight_std = np.std(C_8_ensemble - CH4_not_ads_1_ensemble)
nine_std = np.std(CO2_9_ensemble - CH4_not_ads_1_ensemble)
ten_std = np.std(NiCeO2_10_ensemble - CH4_not_ads_1_ensemble)

fig, ax = plt.subplots(figsize = (10,6))
ax.tick_params(bottom=False)
ax.plot(range(5),[one, two, three, four, five],'b_', markersize=30, mew=2, label = 'expected value')
ax.plot(range(5),[one+2*one_std, two+2*two_std, three+2*three_std, four+2*four_std, five+2*five_std],'g_', markersize=30, mew=2, label = '95% confidence') #upper
ax.plot(range(5),[one-2*one_std, two-2*two_std, three-2*three_std, four-2*four_std, five-2*five_std],'g_', markersize=30, mew=2)
ax.plot(0,one,'k_', markersize=30, mew=2)
ax.set_ylabel('Relative free energy (eV)')
ax.legend(loc='best')
fig.tight_layout()
fig.savefig('pathway_part_1.png',dpi=220)

fig1, ax1 = plt.subplots(figsize = (10,6))
ax1.tick_params(bottom=False)
ax1.set_xticks(np.arange(0, 5, step=1.0))
ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
ax1.plot(range(5),[six, seven, eight, nine, ten],'b_', markersize = 30, mew=2, label = 'expected value')
ax1.plot(range(5),[six+2*six_std, seven+2*seven_std, eight+2*eight_std, nine+2*nine_std, ten+2*ten_std],'g_', markersize = 30, mew=2, label = '95% confidence')
ax1.plot(range(5),[six-2*six_std, seven-2*seven_std, eight-2*eight_std, nine-2*nine_std, ten-2*ten_std],'g_', markersize = 30, mew=2, label = '95% confidence')
ax1.set_ylabel('Relative free energy (eV)')
fig1.tight_layout()
fig1.savefig('pathway_part_2.png',dpi=220)


############################ CH2 versus methoxy, starting from CH3
CH3_H_1 = dict_ads1['CH3_H'][1]
CH2_2H_2 = dict_ads1['CH2_2H'][1]
methoxy_3 = dict_ads1['methoxy'][1]

CH3_H_1_ensemble = dict_ads1['CH3_H'][2]
CH2_2H_2_ensemble = dict_ads1['CH2_2H'][2]
methoxy_3_ensemble = dict_ads1['methoxy'][2]

one = CH3_H_1 - CH3_H_1
two = CH2_2H_2 - CH3_H_1
three = methoxy_3 - CH3_H_1
print('CH3_H_1',CH3_H_1)
print('CH2_2H_2',CH2_2H_2)
print('methoxy_3',methoxy_3)
print('one',one)
print('two',two)
print('three',three)
one_std = np.std(CH3_H_1_ensemble - CH3_H_1_ensemble)
two_std = np.std(CH2_2H_2_ensemble - CH3_H_1_ensemble)
three_std = np.std(methoxy_3_ensemble - CH3_H_1_ensemble)

fig2, ax2 = plt.subplots(figsize = (8,5))
ax2.tick_params(bottom=False)
ax2.set_xticks(np.arange(0, 5, step=1.0))
ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
ax2.plot(range(1,2),[two],'b+', markersize = 30, mew=2, label='methylene')
ax2.plot(range(1,2),[two+2*two_std],'b_', markersize = 30, mew=2, label='methylene 95% confidence')
ax2.plot(range(1,2),[two-2*two_std],'b_', markersize = 30, mew=2)
ax2.plot(range(1,2),[three],'+', color='tab:orange', markersize = 30, mew=2, label='methoxy')
ax2.plot(range(1,2),[three+2*three_std],'_', color='tab:orange', markersize = 30, mew=2, label='methoxy 95% confidence')
ax2.plot(range(1,2),[three-2*three_std],'_', color='tab:orange', markersize = 30, mew=2)
ax2.plot(range(1),[one],'k_', markersize = 30, mew=2)
ax2.set_ylabel('Relative free energy (eV)')
ax2.legend(loc='best')
fig2.tight_layout()
fig2.savefig('methyl_to_methoxy_or_methylene.png',dpi=220)


############################ Dehydrogenation in the presense of two and four O's

two_O_CH3 = dict_ads2['2O_CH3'][1]
two_O_CH2 = dict_ads2['2O_CH2'][1]
two_O_CH = dict_ads2['2O_CH'][1]
two_O_C = dict_ads2['2O_C'][1]

four_O_CH3 = dict_ads2['4O_CH3'][1]
four_O_CH2 = dict_ads2['4O_CH2'][1]
four_O_CH = dict_ads2['4O_CH'][1]
four_O_C = dict_ads2['4O_C'][1]

two_O_CH3_ensemble = dict_ads2['2O_CH3'][2]
two_O_CH2_ensemble = dict_ads2['2O_CH2'][2]
two_O_CH_ensemble = dict_ads2['2O_CH'][2]
two_O_C_ensemble = dict_ads2['2O_C'][2]

four_O_CH3_ensemble = dict_ads2['4O_CH3'][2]
four_O_CH2_ensemble = dict_ads2['4O_CH2'][2]
four_O_CH_ensemble = dict_ads2['4O_CH'][2]
four_O_C_ensemble = dict_ads2['4O_C'][2]

one = two_O_CH3 - two_O_CH3
two = two_O_CH2 - two_O_CH3
three = two_O_CH - two_O_CH3
four = two_O_C - two_O_CH3
five = four_O_CH3 - four_O_CH3
six = four_O_CH2 - four_O_CH3
seven = four_O_CH - four_O_CH3
eight = four_O_C - four_O_CH3

print('2O_two',two)
print('2O_three',three)
print('2O_four',four)
one_std = np.std(two_O_CH3_ensemble - two_O_CH3_ensemble)
two_std = np.std(two_O_CH2_ensemble - two_O_CH3_ensemble)
three_std = np.std(two_O_CH_ensemble - two_O_CH3_ensemble)
four_std = np.std(two_O_C_ensemble - two_O_CH3_ensemble)
five_std = np.std(four_O_CH3_ensemble - four_O_CH3_ensemble)
six_std = np.std(four_O_CH2_ensemble - four_O_CH3_ensemble)
seven_std = np.std(four_O_CH_ensemble - four_O_CH3_ensemble)
eight_std = np.std(four_O_C_ensemble - four_O_CH3_ensemble)

plt.rcParams.update({'font.size': 18})
fig3, ax3 = plt.subplots(figsize = (10,6))
ax3.tick_params(bottom=False)
ax3.set_xticks(np.arange(0, 5, step=1.0))
ax3.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
ax3.plot(range(1,4),[two, three, four],'b+', markersize = 30, mew=2, label='2 O pathway')
ax3.plot(range(1,4),[two+2*two_std, three+2*three_std, four+2*four_std],'b_', markersize = 30, mew=2, label='2 O pathway 95% confidence')
ax3.plot(range(1,4),[two-2*two_std, three-2*three_std, four-2*four_std],'b_', markersize = 30, mew=2)
ax3.plot(range(1,4),[six, seven, eight],'+', color='tab:orange', markersize = 30, mew=2, label='4 O pathway')
ax3.plot(range(1,4),[six+2*six_std, seven+2*seven_std, eight+2*eight_std],'_', color='tab:orange', markersize = 30, mew=2, label='4 O pathway 95% confidence')
ax3.plot(range(1,4),[six-2*six_std, seven-2*seven_std, eight-2*eight_std],'_', color='tab:orange', markersize = 30, mew=2)
ax3.plot(range(1),[one],'k_', markersize = 30, mew=2)
ax3.set_ylabel('Relative free energy (eV)')
ax3.legend(loc='best')
fig3.tight_layout()
fig3.savefig('dehydrogenation_4O_and_2O.png',dpi=220)


